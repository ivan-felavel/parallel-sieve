Criba de Eratóstenes
==========
En este proyecto se presentan versiones del algoritmo usando programación con memoria distribuida y compartida, utilizando tres API's; **MPI**, para la memoria distribuida, **OpenMP** y **Pthreads**, para la memoria compartida. Todas estas usando las bibliotecas correspondientes en  **C++11** y la **STL**.
### Elementos importantes
Hay dos estructuras importantes, un conjunto de bits que sirve como mapa para determinar si un número es primo o no. El número sirve como ínidice (llave) y el valor es 1 o 0 (1 si es primo, 0 si no). Esta estructura sirve para tachar los número no primos. La otra estructura es un vector que almacena los números primos.
## MPI
El método base completo está en el siguiente enlace [Parallelization: Sieve of	Eratosthenes By Aaron Weeden](http://www.shodor.org/media/content//petascale/materials/UPModules/sieveOfEratosthenes/module_document_pdf.pdf).  Todos los procesos calculan los números primos  hasta la raíz de **n**, sin embargo, el cálculo de los números entre la raíz y **n** se divide entre los procesos. Cada proceso tiene un arreglo que sirve con el mismo propósito que el conjunto de bits, tachar los números no primos en un intervalo definido como sigue
* Elementos por proceso = **n** / número de procesos
* Índice de inicio = Número de proceso * Elementos por proceso + Raíz de **n** + 1
* Índice final = Índice de inicio + Elementos por proceso - 1

Entonces, cada proceso marca los número no primos en el intervalo que le corresponde. Después el proceso maestro se encarga de mostrar los resultados. Imprime los números que almacenó como primos desde 2 hasta raíz de **n** + Elementos por proceso, luego recibe de cada proceso el arreglo con los número no primos que calculó.

### Compilación
```sh
mpic++ sieveMPI2.cpp -o sieveMPI
```
### Ejecución 
```sh
mpirun -n número de procesos sieveMPI
```

### Errores
Un error que no se ha podido corregir es que el programa no calcula todos los números primos menores o iguales a **n** cuando la cantidad de números entre la raíz de **n**  y **n** no es divisible entre el número de procesos.

## Pthreads
La forma en la que se obtienen los número primos difiere de MPI ya que aquí la memoria es compartida. El hilo principal se encarga de encontrar **k** números primos, de tal forma que **k** sea igual al número de hilos. Así a cada hilo se le asigna un número primo y este se encargará de marcar los múltiplos del primo que le corresponde. Después se unen los resultados. Un hilo puede terminar su trabajo antes que los demás y la lista de números no estaría ordenada, es por eso que se usa una cola de prioridad para almacenar los números primos y mantener en el frente de la lista al primo máximo menor o igual a **n**. La escritura de un número en la cola de prioridad es una sección crítica por lo que fue necesaria una variable mutex.

### Compilación
```sh
g++ --std=c++11 sievePthreads.cpp -o sievePthread -lpthread
```
### Ejecución 
```sh
./sievePthread número de procesos
```
## OPENMP

Por las facilidades que brinda OpenMP, solo se usó la función `# pragma omp parallel for `, para paralelizar el marcado de número no primos. Por lo que la parte esencial de la criba quedó como sigue:

```c++

	for (long long l i = 2; i <= n + 1; i++) {
		if (esPrimo[i]) {
			# pragma omp parallel for num_threads(thread_count) 
			for (long long j = i * i; j <= n + 1; j += i) {
				esPrimo[j] = 0;
			}
			listaPrimos.push_back(i);
		}
	}
```

### Compilación
```sh
 g++ --std=c++11 -Wall sieveOpenMP.cpp -o sieveomp -fopenmp
```
### Ejecución 
```sh
./sieveomp número de procesos
```
## Tabla comparativa
| API      | Valor de n | Número de Procesos | Tiempo (segundos) |
|----------|------------|--------------------|------------------|
| MPI      | 1000000    | 7                  | 1.035            |
| Pthreads | 1000000    | 7                  | 1.335            |
| OpenMP   | 1000000    | 7                  | 1.287            |


			





